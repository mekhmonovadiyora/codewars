var countBits = function (n) {
  let remainder;
  let binary = 0;

  while (n > 0) {
    remainder = n % 2;
    if (remainder == 1) {
      binary++;
    }
    n = Math.trunc(n / 2);
  }

  return binary;
};

console.log(countBits(7));
